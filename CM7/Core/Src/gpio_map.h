#ifndef GPIO_MAP_H
#define GPIO_MAP_H


/*
 * Map of IPM GPIO banks
 */
#define IPM_0_GPIO_BANK  GPIOI
#define IPM_1_GPIO_BANK  GPIOI
#define IPM_2_GPIO_BANK  GPIOF
#define IPM_3_GPIO_BANK  GPIOI
#define IPM_4_GPIO_BANK  GPIOI
#define IPM_5_GPIO_BANK  GPIOI
#define IPM_6_GPIO_BANK  GPIOF
#define IPM_7_GPIO_BANK  GPIOG
#define IPM_8_GPIO_BANK  GPIOC
#define IPM_9_GPIO_BANK  GPIOF
#define IPM_10_GPIO_BANK GPIOA
#define IPM_11_GPIO_BANK GPIOE
#define IPM_12_GPIO_BANK GPIOF
#define IPM_13_GPIO_BANK GPIOC
#define IPM_14_GPIO_BANK GPIOG
#define IPM_15_GPIO_BANK GPIOJ

/*
 * Map of USER_IO GPIO banks
 */
#define USER_IO_0_GPIO_BANK  GPIOE
#define USER_IO_1_GPIO_BANK  GPIOE
#define USER_IO_2_GPIO_BANK  GPIOC
#define USER_IO_3_GPIO_BANK  GPIOH
#define USER_IO_4_GPIO_BANK  GPIOA
#define USER_IO_5_GPIO_BANK  GPIOA
#define USER_IO_6_GPIO_BANK  GPIOB
#define USER_IO_7_GPIO_BANK  GPIOG
#define USER_IO_8_GPIO_BANK  GPIOA
#define USER_IO_9_GPIO_BANK  GPIOA
#define USER_IO_10_GPIO_BANK GPIOI
#define USER_IO_11_GPIO_BANK GPIOJ
#define USER_IO_12_GPIO_BANK GPIOB
#define USER_IO_13_GPIO_BANK GPIOJ
#define USER_IO_14_GPIO_BANK GPIOB
#define USER_IO_15_GPIO_BANK GPIOJ
#define USER_IO_16_GPIO_BANK GPIOJ
#define USER_IO_17_GPIO_BANK GPIOG
#define USER_IO_18_GPIO_BANK GPIOF
#define USER_IO_19_GPIO_BANK GPIOF
#define USER_IO_20_GPIO_BANK GPIOE
#define USER_IO_21_GPIO_BANK GPIOG
#define USER_IO_22_GPIO_BANK GPIOE
#define USER_IO_23_GPIO_BANK GPIOE
#define USER_IO_24_GPIO_BANK GPIOE
#define USER_IO_25_GPIO_BANK GPIOE
#define USER_IO_26_GPIO_BANK GPIOE
#define USER_IO_27_GPIO_BANK GPIOB
#define USER_IO_28_GPIO_BANK GPIOE
#define USER_IO_29_GPIO_BANK GPIOE
#define USER_IO_30_GPIO_BANK GPIOH
#define USER_IO_31_GPIO_BANK GPIOJ
#define USER_IO_32_GPIO_BANK GPIOH
#define USER_IO_33_GPIO_BANK GPIOB
#define USER_IO_34_GPIO_BANK GPIOA

/*
 * Map of IPM GPIO pin numbers
 */
#define IPM_0_GPIO_PIN_NUM  GPIO_PIN_4
#define IPM_1_GPIO_PIN_NUM  GPIO_PIN_7
#define IPM_2_GPIO_PIN_NUM  GPIO_PIN_2
#define IPM_3_GPIO_PIN_NUM  GPIO_PIN_12
#define IPM_4_GPIO_PIN_NUM  GPIO_PIN_13
#define IPM_5_GPIO_PIN_NUM  GPIO_PIN_14
#define IPM_6_GPIO_PIN_NUM  GPIO_PIN_3
#define IPM_7_GPIO_PIN_NUM  GPIO_PIN_5
#define IPM_8_GPIO_PIN_NUM  GPIO_PIN_6
#define IPM_9_GPIO_PIN_NUM  GPIO_PIN_4
#define IPM_10_GPIO_PIN_NUM GPIO_PIN_0
#define IPM_11_GPIO_PIN_NUM GPIO_PIN_10
#define IPM_12_GPIO_PIN_NUM GPIO_PIN_5
#define IPM_13_GPIO_PIN_NUM GPIO_PIN_7
#define IPM_14_GPIO_PIN_NUM GPIO_PIN_14
#define IPM_15_GPIO_PIN_NUM GPIO_PIN_0

/*
 * Map of USER_IO GPIO pin numbers
 */
#define USER_IO_0_GPIO_PIN_NUM  GPIO_PIN_1
#define USER_IO_1_GPIO_PIN_NUM  GPIO_PIN_0
#define USER_IO_2_GPIO_PIN_NUM  GPIO_PIN_0
#define USER_IO_3_GPIO_PIN_NUM  GPIO_PIN_4
#define USER_IO_4_GPIO_PIN_NUM  GPIO_PIN_6
#define USER_IO_5_GPIO_PIN_NUM  GPIO_PIN_5
#define USER_IO_6_GPIO_PIN_NUM  GPIO_PIN_5
#define USER_IO_7_GPIO_PIN_NUM  GPIO_PIN_8
#define USER_IO_8_GPIO_PIN_NUM  GPIO_PIN_3
#define USER_IO_9_GPIO_PIN_NUM  GPIO_PIN_4
#define USER_IO_10_GPIO_PIN_NUM GPIO_PIN_15
#define USER_IO_11_GPIO_PIN_NUM GPIO_PIN_1
#define USER_IO_12_GPIO_PIN_NUM GPIO_PIN_0
#define USER_IO_13_GPIO_PIN_NUM GPIO_PIN_3
#define USER_IO_14_GPIO_PIN_NUM GPIO_PIN_2
#define USER_IO_15_GPIO_PIN_NUM GPIO_PIN_2
#define USER_IO_16_GPIO_PIN_NUM GPIO_PIN_4
#define USER_IO_17_GPIO_PIN_NUM GPIO_PIN_1
#define USER_IO_18_GPIO_PIN_NUM GPIO_PIN_12
#define USER_IO_19_GPIO_PIN_NUM GPIO_PIN_11
#define USER_IO_20_GPIO_PIN_NUM GPIO_PIN_8
#define USER_IO_21_GPIO_PIN_NUM GPIO_PIN_0
#define USER_IO_22_GPIO_PIN_NUM GPIO_PIN_7
#define USER_IO_23_GPIO_PIN_NUM GPIO_PIN_9
#define USER_IO_24_GPIO_PIN_NUM GPIO_PIN_13
#define USER_IO_25_GPIO_PIN_NUM GPIO_PIN_11
#define USER_IO_26_GPIO_PIN_NUM GPIO_PIN_14
#define USER_IO_27_GPIO_PIN_NUM GPIO_PIN_10
#define USER_IO_28_GPIO_PIN_NUM GPIO_PIN_12
#define USER_IO_29_GPIO_PIN_NUM GPIO_PIN_15
#define USER_IO_30_GPIO_PIN_NUM GPIO_PIN_7
#define USER_IO_31_GPIO_PIN_NUM GPIO_PIN_5
#define USER_IO_32_GPIO_PIN_NUM GPIO_PIN_8
#define USER_IO_33_GPIO_PIN_NUM GPIO_PIN_14
#define USER_IO_34_GPIO_PIN_NUM GPIO_PIN_10

/*
 * Map of GPIO banks for specifics IOs
 */
#define BLUE_LED_GPIO_BANK         GPIOD
#define LED_0_GPIO_BANK            GPIOD
#define LED_1_GPIO_BANK            GPIOD
#define LED_2_GPIO_BANK            GPIOD
#define EN_12V_GPIO_BANK           GPIOD
#define PAYLOAD_RESET_GPIO_BANK    GPIOB
#define ALARM_A_GPIO_BANK          GPIOF
#define ALARM_B_GPIO_BANK          GPIOD
#define PWR_GOOD_A_GPIO_BANK       GPIOH
#define PWR_GOOD_B_GPIO_BANK       GPIOB
#define MASTER_TCK_GPIO_BANK       GPIOH
#define MASTER_TDI_GPIO_BANK       GPIOJ
#define MASTER_TRST_GPIO_BANK      GPIOD
#define MASTER_TDO_GPIO_BANK       GPIOD
#define MASTER_TMS_GPIO_BANK       GPIOJ
#define IPMB_A_EN_GPIO_BANK        GPIOG
#define IPMB_A_RDY_GPIO_BANK       GPIOK
#define IPMB_B_EN_GPIO_BANK        GPIOG
#define IPMB_B_RDY_GPIO_BANK       GPIOG
#define HANDLE_GPIO_BANK           GPIOB
#define HW_0_GPIO_BANK             GPIOH
#define HW_1_GPIO_BANK             GPIOJ
#define HW_2_GPIO_BANK             GPIOH
#define HW_3_GPIO_BANK             GPIOJ
#define HW_4_GPIO_BANK             GPIOJ
#define HW_5_GPIO_BANK             GPIOK
#define HW_6_GPIO_BANK             GPIOJ
#define HW_7_GPIO_BANK             GPIOK

/*
 * Map of GPIO pin numbers for specifics IOs
 */
#define BLUE_LED_GPIO_PIN_NUM         GPIO_PIN_8
#define LED_0_GPIO_PIN_NUM            GPIO_PIN_10
#define LED_1_GPIO_PIN_NUM            GPIO_PIN_9
#define LED_2_GPIO_PIN_NUM            GPIO_PIN_12
#define EN_12V_GPIO_PIN_NUM           GPIO_PIN_11
#define PAYLOAD_RESET_GPIO_PIN_NUM    GPIO_PIN_15
#define ALARM_A_GPIO_PIN_NUM          GPIO_PIN_13
#define ALARM_B_GPIO_PIN_NUM          GPIO_PIN_15
#define PWR_GOOD_A_GPIO_PIN_NUM       GPIO_PIN_12
#define PWR_GOOD_B_GPIO_PIN_NUM       GPIO_PIN_13
#define MASTER_TCK_GPIO_PIN_NUM       GPIO_PIN_11
#define MASTER_TDI_GPIO_PIN_NUM       GPIO_PIN_7
#define MASTER_TRST_GPIO_PIN_NUM      GPIO_PIN_13
#define MASTER_TDO_GPIO_PIN_NUM       GPIO_PIN_14
#define MASTER_TMS_GPIO_PIN_NUM       GPIO_PIN_6
#define IPMB_A_EN_GPIO_PIN_NUM        GPIO_PIN_2
#define IPMB_A_RDY_GPIO_PIN_NUM       GPIO_PIN_2
#define IPMB_B_EN_GPIO_PIN_NUM        GPIO_PIN_4
#define IPMB_B_RDY_GPIO_PIN_NUM       GPIO_PIN_3
#define HANDLE_GPIO_PIN_NUM           GPIO_PIN_12
#define HW_0_GPIO_PIN_NUM             GPIO_PIN_9
#define HW_1_GPIO_PIN_NUM             GPIO_PIN_10
#define HW_2_GPIO_PIN_NUM             GPIO_PIN_10
#define HW_3_GPIO_PIN_NUM             GPIO_PIN_11
#define HW_4_GPIO_PIN_NUM             GPIO_PIN_8
#define HW_5_GPIO_PIN_NUM             GPIO_PIN_0
#define HW_6_GPIO_PIN_NUM             GPIO_PIN_9
#define HW_7_GPIO_PIN_NUM             GPIO_PIN_1

/*
 * Set the output state of IPM and USER_IO pins
 *
 * NUM argument must be:
 *     IPM number, from 0 to 15
 *     USER_IO number, from 0 to 34
 *
 * X argument must:
 *     SET:   output to HIGH
 *     RESET: output to LOW
 *
 * This macro generates the command as following:
 *     HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET)
 *
 * It uses "bank" and "pin" mapping defined above
 */
#define IPM_SET_STATE(NUM, X)     HAL_GPIO_WritePin(    IPM_ ## NUM ## _GPIO_BANK,     IPM_ ## NUM ## _GPIO_PIN_NUM, GPIO_PIN_ ## X)
#define USER_IO_SET_STATE(NUM, X) HAL_GPIO_WritePin(USER_IO_ ## NUM ## _GPIO_BANK, USER_IO_ ## NUM ## _GPIO_PIN_NUM, GPIO_PIN_ ## X)


/*
 * Read the input state of IPM and USER_IO pins
 *
 * NUM argument must be:
 *     IPM number, from 0 to 15
 *     USER_IO number, from 0 to 34
 *
 * This macro generates the command as following:
 *     HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_7)
 *
 * It uses "bank" and "pin" mapping defined above
 *
 * Return value (follows HAL_GPIO specs):
 *     GPIO_PIN_SET
 *     GPIO_PIN_RESET
 *
 */
#define IPM_GET_STATE(NUM)     HAL_GPIO_ReadPin(    IPM_ ## NUM ## _GPIO_BANK,     IPM_ ## NUM ## _GPIO_PIN_NUM)
#define USER_IO_GET_STATE(NUM) HAL_GPIO_ReadPin(USER_IO_ ## NUM ## _GPIO_BANK, USER_IO_ ## NUM ## _GPIO_PIN_NUM)



/*
 * Set the configuration of IPM and USER_IO pins
 *
 * NUM argument must be:
 *     IPM number, from 0 to 15
 *     USER_IO number, from 0 to 34
 *
 * MODE follows HAL_GPIO specs
 *
 * PULL follows HAL_GPIO specs
 *
 */
#define IPM_CONFIGURE_PIN(NUM, MODE, PULL) \
            { \
	            GPIO_InitTypeDef GPIO_InitStruct = {0}; \
                GPIO_InitStruct.Pin = IPM_ ## NUM ## _GPIO_PIN_NUM;\
                GPIO_InitStruct.Mode = MODE;\
                GPIO_InitStruct.Pull = PULL;\
                GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;\
                HAL_GPIO_Init(IPM_ ## NUM ## _GPIO_BANK, &GPIO_InitStruct);\
            }
#define USER_IO_CONFIGURE_PIN(NUM, MODE, PULL) \
            { \
	            GPIO_InitTypeDef GPIO_InitStruct = {0}; \
                GPIO_InitStruct.Pin = USER_IO_ ## NUM ## _GPIO_PIN_NUM;\
                GPIO_InitStruct.Mode = MODE;\
                GPIO_InitStruct.Pull = PULL;\
                GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;\
                HAL_GPIO_Init(USER_IO_ ## NUM ## _GPIO_BANK, &GPIO_InitStruct);\
            }


/*
 * Set specific Outputs
 */
#define BLUE_LED_SET_STATE(X)          HAL_GPIO_WritePin( BLUE_LED_GPIO_BANK     , BLUE_LED_GPIO_PIN_NUM     , GPIO_PIN_ ## X)
#define LED_0_SET_STATE(X)             HAL_GPIO_WritePin( LED_0_GPIO_BANK        , LED_0_GPIO_PIN_NUM        , GPIO_PIN_ ## X)
#define LED_1_SET_STATE(X)             HAL_GPIO_WritePin( LED_1_GPIO_BANK        , LED_1_GPIO_PIN_NUM        , GPIO_PIN_ ## X)
#define LED_2_SET_STATE(X)             HAL_GPIO_WritePin( LED_2_GPIO_BANK        , LED_2_GPIO_PIN_NUM        , GPIO_PIN_ ## X)
#define EN_12V_SET_STATE(X)            HAL_GPIO_WritePin( EN_12V_GPIO_BANK       , EN_12V_GPIO_PIN_NUM       , GPIO_PIN_ ## X)
#define PAYLOAD_RESET_SET_STATE(X)     HAL_GPIO_WritePin( PAYLOAD_RESET_GPIO_BANK, PAYLOAD_RESET_GPIO_PIN_NUM, GPIO_PIN_ ## X)

//Template
//#define CACHORRO_SET_STATE(X)        HAL_GPIO_WritePin(CACHORRO_GPIO_BANK, CACHORRO_GPIO_PIN_NUM, GPIO_PIN_ ## X)



/*
 * Read specific Inputs
 */
#define IPMB_A_RDY_GET_STATE()     HAL_GPIO_ReadPin( IPMB_A_RDY_GPIO_BANK , IPMB_A_RDY_GPIO_PIN_NUM  )
#define IPMB_B_RDY_GET_STATE()     HAL_GPIO_ReadPin( IPMB_B_RDY_GPIO_BANK , IPMB_B_RDY_GPIO_PIN_NUM  )
#define HANDLE_GET_STATE()         HAL_GPIO_ReadPin( HANDLE_GPIO_BANK     , HANDLE_GPIO_PIN_NUM      )
#define HW_0_GET_STATE()           HAL_GPIO_ReadPin( HW_0_GPIO_BANK       , HW_0_GPIO_PIN_NUM        )
#define HW_1_GET_STATE()           HAL_GPIO_ReadPin( HW_1_GPIO_BANK       , HW_1_GPIO_PIN_NUM        )
#define HW_2_GET_STATE()           HAL_GPIO_ReadPin( HW_2_GPIO_BANK       , HW_2_GPIO_PIN_NUM        )
#define HW_3_GET_STATE()           HAL_GPIO_ReadPin( HW_3_GPIO_BANK       , HW_3_GPIO_PIN_NUM        )
#define HW_4_GET_STATE()           HAL_GPIO_ReadPin( HW_4_GPIO_BANK       , HW_4_GPIO_PIN_NUM        )
#define HW_5_GET_STATE()           HAL_GPIO_ReadPin( HW_5_GPIO_BANK       , HW_5_GPIO_PIN_NUM        )
#define HW_6_GET_STATE()           HAL_GPIO_ReadPin( HW_6_GPIO_BANK       , HW_6_GPIO_PIN_NUM        )
#define HW_7_GET_STATE()           HAL_GPIO_ReadPin( HW_7_GPIO_BANK       , HW_7_GPIO_PIN_NUM        )

//Template
//#define CACHORRO_GET_STATE(X)        HAL_GPIO_ReadPin(CACHORRO_GPIO_BANK, CACHORRO_GPIO_PIN_NUM)



/*
 * The following signals are being considered output for testing purposes
 */

#define ALARM_A_SET_STATE(X)           HAL_GPIO_WritePin( ALARM_A_GPIO_BANK     , ALARM_A_GPIO_PIN_NUM    , GPIO_PIN_ ## X)
#define ALARM_B_SET_STATE(X)           HAL_GPIO_WritePin( ALARM_B_GPIO_BANK     , ALARM_B_GPIO_PIN_NUM    , GPIO_PIN_ ## X)
#define PWR_GOOD_A_SET_STATE(X)        HAL_GPIO_WritePin( PWR_GOOD_A_GPIO_BANK  , PWR_GOOD_A_GPIO_PIN_NUM , GPIO_PIN_ ## X)
#define PWR_GOOD_B_SET_STATE(X)        HAL_GPIO_WritePin( PWR_GOOD_B_GPIO_BANK  , PWR_GOOD_B_GPIO_PIN_NUM , GPIO_PIN_ ## X)
#define MASTER_TCK_SET_STATE(X)        HAL_GPIO_WritePin( MASTER_TCK_GPIO_BANK  , MASTER_TCK_GPIO_PIN_NUM , GPIO_PIN_ ## X)
#define MASTER_TDI_SET_STATE(X)        HAL_GPIO_WritePin( MASTER_TDI_GPIO_BANK  , MASTER_TDI_GPIO_PIN_NUM , GPIO_PIN_ ## X)
#define MASTER_TRST_SET_STATE(X)       HAL_GPIO_WritePin( MASTER_TRST_GPIO_BANK , MASTER_TRST_GPIO_PIN_NUM, GPIO_PIN_ ## X)
#define MASTER_TDO_SET_STATE(X)        HAL_GPIO_WritePin( MASTER_TDO_GPIO_BANK  , MASTER_TDO_GPIO_PIN_NUM , GPIO_PIN_ ## X)
#define MASTER_TMS_SET_STATE(X)        HAL_GPIO_WritePin( MASTER_TMS_GPIO_BANK  , MASTER_TMS_GPIO_PIN_NUM , GPIO_PIN_ ## X)


#endif //GPIO_MAP_H
